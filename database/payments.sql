-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2019 at 07:02 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `agreement`
--

CREATE TABLE `agreement` (
  `id` int(11) NOT NULL,
  `first_partyname` varchar(500) NOT NULL,
  `second_partyname` varchar(500) NOT NULL,
  `start_date` varchar(500) NOT NULL,
  `end_date` varchar(500) NOT NULL,
  `agreement_type` varchar(500) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `merchant` varchar(500) NOT NULL,
  `currency` varchar(500) NOT NULL,
  `country` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE `merchant` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `type` varchar(500) NOT NULL,
  `mid` varchar(500) NOT NULL,
  `supervisor` varchar(500) NOT NULL,
  `currency` varchar(500) NOT NULL,
  `contact_person` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `address` varchar(500) NOT NULL,
  `credit` varchar(500) NOT NULL,
  `amount` varchar(500) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `contact` varchar(250) NOT NULL,
  `company` text NOT NULL,
  `gstn` text NOT NULL,
  `add1` text NOT NULL,
  `add2` text NOT NULL,
  `city` varchar(250) NOT NULL,
  `pincode` int(11) NOT NULL,
  `state` varchar(250) NOT NULL,
  `country` int(250) NOT NULL,
  `remark` text NOT NULL,
  `adjust` text NOT NULL,
  `total` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `date`, `name`, `email`, `contact`, `company`, `gstn`, `add1`, `add2`, `city`, `pincode`, `state`, `country`, `remark`, `adjust`, `total`, `created_by`, `created_on`, `status`) VALUES
(2, '0000-00-00', 'nam', 'email@manil', '9876543110', 'comapny', 'GSTIn', 'line1', 'line2', 'city', 600031, 'design', 0, 'remark', '10', 1000, 0, '2019-06-12 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_items`
--

CREATE TABLE `payment_items` (
  `id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `product` text NOT NULL,
  `qnty` float NOT NULL,
  `rate` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pin`
--

CREATE TABLE `pin` (
  `id` int(11) NOT NULL,
  `code` text NOT NULL,
  `terminal_id` text NOT NULL,
  `used_on` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `validity` int(11) NOT NULL,
  `expired_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `code` varchar(500) NOT NULL,
  `service` varchar(500) NOT NULL,
  `operator` varchar(500) NOT NULL,
  `currency` varchar(500) NOT NULL,
  `price` varchar(500) NOT NULL,
  `min_price` varchar(500) NOT NULL,
  `max_price` varchar(500) NOT NULL,
  `cost` varchar(500) NOT NULL,
  `percentage` varchar(500) NOT NULL,
  `member` varchar(500) NOT NULL,
  `calculate_price` varchar(500) NOT NULL,
  `active` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `code` varchar(500) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `session_id` varchar(500) NOT NULL,
  `tid` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `session_id` varchar(250) NOT NULL,
  `terminal_id` varchar(250) NOT NULL,
  `merchant_name` text NOT NULL,
  `branch_name` text NOT NULL,
  `total_cash` float NOT NULL,
  `open_date` datetime NOT NULL,
  `oneq` int(11) NOT NULL,
  `fiveq` int(11) NOT NULL,
  `tenq` int(11) NOT NULL,
  `fiftyq` int(11) NOT NULL,
  `hundredq` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `session_id`, `terminal_id`, `merchant_name`, `branch_name`, `total_cash`, `open_date`, `oneq`, `fiveq`, `tenq`, `fiftyq`, `hundredq`, `status`) VALUES
(0, '123', 'T123', 'Name', 'Branch', 100, '2019-06-17 00:00:00', 2, 5, 10, 5, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `session_check`
--

CREATE TABLE `session_check` (
  `id` int(11) NOT NULL,
  `terminal` text NOT NULL,
  `total_amount` float NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terminal`
--

CREATE TABLE `terminal` (
  `id` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `branch` varchar(500) NOT NULL,
  `merchant` varchar(500) NOT NULL,
  `serial_no` varchar(500) NOT NULL,
  `health` varchar(500) NOT NULL,
  `date` varchar(500) NOT NULL,
  `ip` varchar(500) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `port` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terminal_status`
--

CREATE TABLE `terminal_status` (
  `id` int(11) NOT NULL,
  `merchant_name` varchar(250) NOT NULL,
  `branch_name` varchar(250) NOT NULL,
  `terminal_name` varchar(250) NOT NULL,
  `installation_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `active` int(11) NOT NULL DEFAULT '1',
  `last_logon` datetime NOT NULL,
  `health` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terminal_status`
--

INSERT INTO `terminal_status` (`id`, `merchant_name`, `branch_name`, `terminal_name`, `installation_date`, `status`, `active`, `last_logon`, `health`) VALUES
(1, 'merchant_name', 'branch_name', 'terminal_name', '2019-06-27 00:00:00', 1, 1, '2019-06-27 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `terminal_id` text NOT NULL,
  `operator_id` text NOT NULL,
  `product_code` text NOT NULL,
  `date` datetime NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_method` text NOT NULL,
  `phone` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `voucher_id` text NOT NULL,
  `pin_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_old`
--

CREATE TABLE `transaction_old` (
  `id` int(11) NOT NULL,
  `key_session` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `service_name` text NOT NULL,
  `merchant_name` text NOT NULL,
  `terminal_id` varchar(250) NOT NULL,
  `branch_name` text NOT NULL,
  `product` text NOT NULL,
  `phone` varchar(250) NOT NULL,
  `rrn_number` varchar(250) NOT NULL,
  `stan` varchar(250) NOT NULL,
  `amount_to_pay` float NOT NULL,
  `pay_in_amount` float NOT NULL,
  `payment_method` varchar(250) NOT NULL,
  `reference_number` text NOT NULL,
  `smart_card_number` text NOT NULL,
  `used_voucher_number` varchar(250) NOT NULL,
  `conformation_id` text NOT NULL,
  `is_vouchered` int(11) NOT NULL DEFAULT '1',
  `hold_date_time` datetime NOT NULL,
  `conf_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_old`
--

INSERT INTO `transaction_old` (`id`, `key_session`, `status`, `service_name`, `merchant_name`, `terminal_id`, `branch_name`, `product`, `phone`, `rrn_number`, `stan`, `amount_to_pay`, `pay_in_amount`, `payment_method`, `reference_number`, `smart_card_number`, `used_voucher_number`, `conformation_id`, `is_vouchered`, `hold_date_time`, `conf_date_time`) VALUES
(0, '12', 12, '121', '212', '1', '11', '12', '11', '2', '2', 212, 12, '121', '11', '1212', '11', '22', 1, '2019-06-27 00:00:00', '2019-06-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(11) NOT NULL,
  `transaction_id` varchar(250) NOT NULL,
  `voucher_id` varchar(250) NOT NULL,
  `amount` float NOT NULL,
  `date` date NOT NULL,
  `expiry` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0= Expired, 1= Active, 2= Claimed',
  `terminalid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `transaction_id`, `voucher_id`, `amount`, `date`, `expiry`, `status`, `terminalid`) VALUES
(3, 'T123', 'V1230', 150, '2019-06-25', '2019-06-29', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agreement`
--
ALTER TABLE `agreement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchant`
--
ALTER TABLE `merchant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_items`
--
ALTER TABLE `payment_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pin`
--
ALTER TABLE `pin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terminal`
--
ALTER TABLE `terminal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terminal_status`
--
ALTER TABLE `terminal_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_old`
--
ALTER TABLE `transaction_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agreement`
--
ALTER TABLE `agreement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `merchant`
--
ALTER TABLE `merchant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_items`
--
ALTER TABLE `payment_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pin`
--
ALTER TABLE `pin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `terminal`
--
ALTER TABLE `terminal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `terminal_status`
--
ALTER TABLE `terminal_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
