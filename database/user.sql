-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2019 at 08:06 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_attempt`
--

CREATE TABLE `login_attempt` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `login_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `notes` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `personal_email` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `designation` int(11) NOT NULL,
  `mobile` int(15) NOT NULL,
  `personal_mobile` int(11) NOT NULL,
  `doj` date NOT NULL,
  `dob` date NOT NULL,
  `location` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `addressline1` varchar(200) NOT NULL,
  `addressline2` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `pincode` int(11) NOT NULL,
  `role` int(3) NOT NULL,
  `status` int(2) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `tid`, `name`, `email`, `personal_email`, `designation`, `mobile`, `personal_mobile`, `doj`, `dob`, `location`, `gender`, `addressline1`, `addressline2`, `city`, `state`, `country`, `pincode`, `role`, `status`, `created_on`, `create_by`) VALUES
(1, 1, 'sarath', 'sarath@elephant.com', '', 0, 1235456789, 0, '2019-06-05', '0000-00-00', 'chennai', '', '', '', '', '', '', 0, 0, 0, '2019-06-02 07:22:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_designation`
--

CREATE TABLE `user_designation` (
  `id` int(11) NOT NULL,
  `destination` varchar(200) NOT NULL,
  `department` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_designation`
--

INSERT INTO `user_designation` (`id`, `destination`, `department`, `status`) VALUES
(1, 'Chief Executive Officer', 'CEO', 1),
(2, 'Chief Operating Officer', 'CEO', 1),
(3, 'Chief Financial Officer', 'Financial', 1),
(4, 'Chief Technology Officer', 'Technology', 1),
(5, 'Chief Marketing Officer', 'Marketing', 1),
(6, 'Chief Legal Officer', 'Lawyer', 1),
(7, 'Accounts Manager', 'Financial', 1),
(8, 'Recruitment Manager ', 'Human Resources', 1),
(9, 'Technology Manager', 'Technology', 1),
(10, 'Store Manager', 'House Keeping', 1),
(11, 'General Managers', 'General Managers', 1),
(12, 'Developer', 'Technology', 1),
(13, 'Senior Developer', 'Technology', 1),
(14, 'Accountants', 'Financial', 1),
(15, 'Business Advisor', 'Sales', 1),
(16, 'Business Executive', 'Sales', 1),
(17, 'Company Secretary', 'Lawyer', 1),
(18, 'Escalation Manager', 'Sales', 1),
(19, 'HR Executive', 'Human Resources', 1),
(20, 'HR Manager', 'Human Resources', 1),
(21, 'HR Recruiter', 'Human Resources', 1),
(22, 'House Keeping', 'House Keeping', 1),
(23, 'Lawyer', 'Lawyer', 1),
(24, 'Marketing Analyst', 'Marketing', 1),
(25, 'Marketing Expert', 'Marketing', 1),
(26, 'Marketing Head', 'Marketing', 1),
(27, 'Network Engineer', 'Technology', 1),
(28, 'Network Manager', 'Technology', 1),
(29, 'Receptionist', 'Marketing', 1),
(30, 'SEO', 'Technology', 1),
(31, 'Security guard', 'House Keeping', 1),
(32, 'Senior Accounts Manager', 'Financial', 1),
(33, 'Senior Business Advisor', 'Sales', 1),
(34, 'Senior Enterprise Manager', 'Sales', 1),
(35, 'Senior HR Manager', 'Human Resources', 1),
(36, 'Support Services Manager', 'Marketing', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role_name`, `status`) VALUES
(1, 'user', 1),
(2, 'manager', 1),
(3, 'admin', 1),
(4, 'super admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_tracking`
--

CREATE TABLE `user_tracking` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `designation` int(11) NOT NULL,
  `create_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_attempt`
--
ALTER TABLE `login_attempt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `user_designation`
--
ALTER TABLE `user_designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tracking`
--
ALTER TABLE `user_tracking`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_attempt`
--
ALTER TABLE `login_attempt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_designation`
--
ALTER TABLE `user_designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_tracking`
--
ALTER TABLE `user_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
