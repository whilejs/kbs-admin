const express=require('express');
let router=express.Router();
let User=require('./model/user');
let Payment=require('./model/payments');
let Vouchers=require('./model/vouchers');
let Terminal=require('./model/terminal');
let Company=require('./model/company');
let Dashboard=require('./model/dashboard-sales');
let Transactions=require('./model/transactions');
let Pin=require('./model/pinloder');

router.all('/api/:method/:action',(req,res)=>{
    let method=req.params.method;
    let action=req.params.action;
    let body={payload:req.body, action:action};
    if(method==="user"){
        //add session object for read and write session
        body.session=req.session; console.log("Session",req.session);
        
        let p=new User(body);        
         p.Output()
             .then(data=>res.send(data))
             .catch(err=>res.send(err));
    }else if(method==="payments"){
        let p=new Payment(body);
         p.Output()
             .then(data=>res.send(data))
             .catch(err=>res.send(err));

    } if(method==="dashboard"){
        let p=new Dashboard(body);
         p.Output()
             .then(data=>res.send(data))
             .catch(err=>res.send(err));

    }else if(method==="transactions"){
        let p=new Transactions(body);
         p.Output()
             .then(data=>res.send(data))
             .catch(err=>res.send(err));

    }else if(method==="terminal"){
        let p=new Terminal(body);
         p.Output()
             .then(data=>res.send(data))
             .catch(err=>res.send(err));

    }else if(method==="vouchers"){
        let p=new Vouchers(body);
         p.Output()
             .then(data=>res.send(data))
             .catch(err=>res.send(err));

    }else if(method==="company"){
        let p=new Company(body);
        p.Output()
            .then(data=>res.send(data))
            .catch(err=>res.send(err));

    }else if(method==="pin"){
        let p=new Pin(body);
        p.Output()
            .then(data=>res.send(data))
            .catch(err=>res.send(err));

    }else{
        res.send("No method found")
    }
});

router.get('/admin/:file',(req,res)=>{
    try {
        let file=req.params.file;
        res.render("admin/"+file);
    }catch (e) {
        res.render("404");
    }
});

router.get('/:file',(req,res)=>{
    let file=req.params.file;
    res.render(file);
});

router.get('/admin/',(req,res)=>{
    res.redirect('/admin/dashboard');
});

router.get('/',(req,res)=>{
    res.redirect('/admin/dashboard');
});



module.exports=router;