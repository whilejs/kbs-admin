const express=require('express');
const path=require('path');
const cors=require('cors');
const session=require('express-session');
const cookieParser=require('cookie-parser');
const bodyParser=require('body-parser');
let router=require('./router');
let apiRouter=require('./api-router');

const app=express();
const port=process.env.PORT || 3000;


app.use(cors());
app.use(session({
    secret: 'bad parrot',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}));

app.use(bodyParser.urlencoded({extend:true,limit: '50mb',parameterLimit: 1000000}));
app.use(bodyParser.json());
app.use(cookieParser());
app.set('views', 'bin/views');
app.set('view engine', 'ejs');
app.use('/',router);
app.use('/', express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next){
    res.render('404', { status: 404, url: req.url });
});
app.use(function(err, req, res, next){
    res.render('500', {
        status: err.status || 500
        , error: err
    });
});

app.listen(port, () => console.log(`Listening on port ${port}`));