const Utility=require('./utility');

class Terminal{
    constructor(data){
        this.data=data;
    }

    Output(){
        let action=this.data.action;
        switch(action){
            case 'get':
                return this.get();
            case 'search':
                return this.search(this.data);
            case 'insert':
                return this.insert(this.data);
            case 'single':
                return this.getsingleuser();
            case 'update':
                return this.update(this.data);
            case 'delete':
                return this.delete(this.data);
            default :
                return Promise.reject("No action found");
        }
    }

    get(){
        return Utility.Database("SELECT * FROM terminal_status order by id desc" );
    }
    search(d){
        return Utility.Database("SELECT * FROM voucher where voucher_id  =  '" +d.payload['delpay']+"'" );
    }
    insert(d){
        return Utility.Database("insert into payments (date, name, email, contact, company, gstn, add1, add2, city, pincode, state, country, remark, adjust, total ,created_on, status   ) values  ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,DATE( DATE_SUB( NOW() , INTERVAL 330 MINUTE ) ),?)",[d.payload['pdate'],d.payload['lname'],d.payload['email'],d.payload['phone'],d.payload['company'],d.payload['gstin'],d.payload['addr1'],d.payload['addr2'],d.payload['city'],d.payload['pincode'],d.payload['state'],d.payload['country'],d.payload['remark'],d.payload['adjs'],d.payload['grtotal'],1 ]);
    }
    getsingleuser(){
        return Utility.Database("SELECT * FROM users WHERE tid=?",[1]);
    }
    update(d){
        console.log(d)
        return Utility.Database("SELECT * FROM payments  where id= "+d.payload['updatepay']);
    }
    delete(d){
        return Utility.Database("delete FROM payments  where id= "+d.payload['delpay']);
    }
}

module.exports=Terminal;
