const con = require('./connection');

class Utility {

    Database(query, data) {
        return new Promise((resolve, reject) => {
            con.query(query, data, (err, results) => {
                if (err) {
                    return reject(err);
                } else {
                    return resolve(results);
                }
            });
           //console.log(sql.sql);
        });
        
    }


    queryBuilder(type, table, data,where=false) {
        if (type) {
            let query = "";
            let keys=Object.keys(data).join(',');
            let values=Object.keys(data).map((key, i) => {
                return `'${data[key]}'`;
            });
            if (type === 'insert') {
                query += `INSERT INTO ${table}(${keys}) VALUES(${values})`;
            }else if(type==="update"){
                query=`UPDATE ${table} SET `;
                Object.keys(data).forEach((k,i)=>{
                    if (i === 0) query+=`${k}='${data[k]}'`;
                    else query+=`,${k}='${data[k]}'`;
                });
                if(where){
                    query+=" where ";
                    Object.keys(where).forEach((k,i)=>{
                        if (i === 0) query+=`${k}='${where[k]}'`;
                        else query+=`and ${k}='${where[k]}'`;
                    });
                }
            }

            return query;
        }else {
            return false;
        }
    }
}

module.exports = new Utility();