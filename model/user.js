const Utility=require('./utility');

class User{
   constructor(data){
      this.data=data;
   }

   Output(){
      let action=this.data.action;
      switch(action){
         case 'getUser':
            return this.getUsers();
         case 'checkuser':
            return this.checkuser();
         case 'passupdate':
            return this.passupdate();
         case 'upadateuser':
            return this.upadateuser();
         case 'insert':
            return this.insertuser();
         case 'insertrole':
            return this.insertrole();
         case 'updaterole':
            return this.updaterole();
         case 'logincheck':
            return this.logincheck();
         case 'getrole':
            return this.getrole();
         case 'getroleactive':
            return this.getroleactive();
         case 'deleterole':
            return this.daleterole();
         case 'single':
            return this.getsingleuser();
         case 'update':
            return this.updateuser();
         case 'getgesigination':
            return this.getDesination();
         default : 
            return Promise.reject("No action found");
      }
   }

   getUsers(){
      return Utility.Database("SELECT us.userid,us.tid,us.name,us.username,us.email,us.mobile,us.doj,us.role,us.status,des.designation,rol.role_name FROM users as us inner join user_designation as des on des.id=us.designation inner join user_role as rol on rol.id=us.role");
   }

   checkuser(){
      let chus=this.data.payload;
      return Utility.Database("SELECT count(*) as tot FROM users as us where us.username=? or us.email=?",[chus.username,chus.email]);
   }
   passupdate(){
      let passup=this.data.payload;
      return Utility.Database("update users set password=? where us.userid=? limit 1",[passup.password,passup.userid]);
   }
   insertuser(){
      let us=this.data.payload;
      return Utility.Database("insert into `users` (`name`, `username`, `email`,`designation`, `mobile`,`doj`, `role`, `status`, `created_on`, `create_by`) VALUES (?,?,?,?,?,?,?,?,?,?)",[us.name,us.username,us.email,us.designation,us.mobile,us.doj,us.role,us.status,us.createon,us.createby]);
   } 
   logincheck(){
      if(this.data.session && this.data.session.userid){
         return Promise.resolve({
            status:1,
            data:this.data.session
         });
      }else{
         let login=this.data.payload;
         return Utility.Database("SELECT userid,name,email,username FROM users as us where us.password=? and (us.email=? or us.username=?)",[login.password,login.username,login.username])
         .then(res=>{
            console.log(JSON.stringify(res));
            if(res && res.length >0){
               let user=res[0];
               //set session
               this.data.session.userid=user.userid;
               this.data.session.username=user.username;
               this.data.session.name=user.name;
               this.data.session.email=user.email;
               this.data.session.save(function(err) {
                  console.log("session error",err);                  
                  return Promise.resolve({
                     status:1,
                     data:user
                  });
                });               
            }else{
               return Promise.resolve({
                  status:0,
                  data:res
               });
            }
                    
         })
         .catch(err=> Promise.resolve({
            status:0,
            data:err
         }));
      }
      
   }
   upadateuser(){
      let updateuser=this.data.payload;
      return Utility.Database("update `users` set status=? where userid=?",[updateuser.status,updateuser.id]);
   }

   getDesination(){
      return Utility.Database("SELECT id,designation from user_designation where status=1 order by designation Asc");
   }

   insertrole(){
      let d=this.data.payload;
      return Utility.Database("INSERT INTO `user_role`(`role_name`, `status`) VALUES (?,?)",[d.rolename,d.status]);
   } 
   updaterole(){
      let d=this.data.payload;
      return Utility.Database("update `user_role` set status=? where id=?",[d.status,d.id]);
   }
   daleterole(id){
      return Utility.Database("delete from `user_role` where id=?",[id]);
   }
   getrole(){
      return Utility.Database("SELECT * FROM user_role");
   }
   getroleactive(){
      return Utility.Database("SELECT * FROM user_role where status=1 order by role_name ASC");
   }
    
   getsingleuser(){
      return Utility.Database("SELECT * FROM users WHERE tid=?",[1]);
   }
   updateuser(){
      return Utility.Database("SELECT * FROM users WHERE tid=?",[1]);
   }   
}

module.exports=User;
