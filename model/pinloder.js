const utility =require('./utility');

class Pinloader{

    constructor(data){
        this.data=data;
    }

    Output(){
        let action=this.data.action;
        switch (action) {
            case "load":
                return this.load();
            default:
                return Promise.resolve("No method found");
        }
    }

    load(){
        let sql="INSERT INTO pin(serial,pin,operator,product) VALUES ?";
        return utility.Database(sql,[this.data.payload.pin]);
    }
}

module.exports=Pinloader;