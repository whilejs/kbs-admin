const Utility=require('./utility');

class Company{
    constructor(data){
        this.data=data;
    }

    Output(){
        let action=this.data.action;
        switch(action){
            case 'insert':
                return this.Insert();
            case 'update':
                return this.Update();
            case 'getTerminal':
                return this.getTerminal();
            case 'getBranch':
                return this.getBranch();
            case 'getMerchant':
                return this.getMerchant();
            case 'getService':
                return this.getService();
            case 'getProduct':
                return this.getProduct();
            case 'bulkUpload':
                return this.bulkUpload();
            case 'getAgreement':
                return this.getAgreement();
            case 'getTalabatUser':
                return this.getTalabatUser();
            case 'getBranchSelect':
                return this.getBranchSelect();
            case 'getMerchantSelect':
                return this.getMerchantSelect();
            case 'getCountryList':
                return this.getCountryList();
            case 'getServiceSelect':
                return this.getServiceSelect();
            case 'getOperator':
                return this.getOperator();
            default :
                return Promise.reject("No action found");
        }
    }

    Insert(){
        let table=this.data.payload.table;
        let data=this.data.payload.data;
        let query=Utility.queryBuilder("insert",table,data);
        return Utility.Database(query,[]);
    }

    Update(){
        let table=this.data.payload.table;
        let data=this.data.payload.data;
        let cond=this.data.payload.cond;
        let query=Utility.queryBuilder("update",table,data,cond);
        return Utility.Database(query,[]);
    }

    getTerminal(){
        return Utility.Database("SELECT * FROM terminal");
    }
    getBranch(){
        return Utility.Database("SELECT * FROM branch");
    }
    getMerchant(){
        return Utility.Database("SELECT * FROM merchant");
    }
    getService(){
        return Utility.Database("SELECT * FROM service");
    }
    getProduct(){
        let sql="SELECT * FROM product";
        if(this.data.payload.filter){
            sql+=` WHERE operator='${this.data.payload.filter}'`;
        }
        return Utility.Database(sql);
    }
    getOperator(){
        let sql="SELECT * FROM operator";
        if(this.data.payload.filter){
            sql+=` WHERE code='${this.data.payload.filter}'`;
        }
        return Utility.Database(sql);
    }
    getAgreement(){
        return Utility.Database("SELECT * FROM agreement");
    }
    getTalabatUser(){
        return Utility.Database("SELECT * FROM talabatUser");
    }
    getBranchSelect(){
        return Utility.Database("SELECT id,branch_name FROM branch");
    }
    getMerchantSelect(){
        return Utility.Database("SELECT id,name FROM merchant");
    }

    bulkUpload(){
        let query = "INSERT INTO pin (serial, pin) VALUES ?";
        let values = this.data.payload.pins;
        return Utility.Database(query,values);
    }
    getCountryList(){
        return Utility.Database("SELECT name FROM countries");
    }
    getServiceSelect(){
        return Utility.Database("SELECT code,name FROM service");
    }

}

module.exports=Company;
